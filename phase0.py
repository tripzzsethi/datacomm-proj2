#!/usr/bin/env python
import asyncore
import logging
import getopt
import sys
from Queue import Queue
from cStringIO import StringIO
from urlparse import urlparse
import time
import socket

done = False
processqueue = Queue()
num_fail = 0
num_sent = 0
num_total = 0
num_con = 0
timelist = list()


def usage():
    print "usage: ./punch -n <numRequests> -c <maxConcurrent> <url>"


def make_request(req_type, what, details, ver='1.1'):
    """ Compose a HTTP request"""
    NL = '\r\n'
    req_line = '{verb} {w} HTTP/{v}'.format(verb=req_type, w=what, v=ver)
    details = [
        "{name}: {v}".format(name=n, v=v) for (n, v) in details.iteritems()
    ]
    detail_lines = NL.join(details)
    full_request = ''.join([req_line, NL, detail_lines, NL, NL])
    return full_request


def parse_url(url, DEFAULT_PORT=80):
    """ Parse a given url into host, path and port
        use DEFAULT_PORT (80) if unspecified
    """
    parse_url = urlparse(url)
    scheme, host, path, port = (parse_url.scheme,
                                parse_url.hostname,
                                parse_url.path,
                                parse_url.port)
    if not port:
        port = DEFAULT_PORT
    if not path:
        path = '/'
    return (scheme, host, path, port)


def extract_header(buff):
    metainf = dict()
    split = buff.split('\r\n\r\n')
    if len(split) != 2:
        return(metainf, 0, None)
    head = split[0]
    content = split[1]
    bytes_head = len(head) + 4

    headlines = head.split('\r\n')
    metainf['Status'] = headlines[0]
    metainf['bytes_head'] = bytes_head

    for lines in headlines[1:]:
        field, value = lines.split(':', 1)
        metainf[field] = value
    return (metainf, bytes_head, content)


def get_status(statusline):
    logging.info(statusline[9:12])
    return int(statusline[9])


def make_punch(n, c, u):
    punch_lst = []
    for i in range(c):
        punch_lst += [OnePunch(u, i)]
    # while len(asyncmap) < c:
    #     dl = processqueue.get()
    #     dl.update_map()
    starttime = time.time()
    run_loop()
    totaltime = time.time() - starttime
    global num_fail
    num_fail = num_total - (num_sent - num_fail)
    print "Time taken for tests: {0:.3f} seconds".format(totaltime)
    print "Completed requests: " + str(num_total - num_fail)
    print "Failed requests: " + str(num_fail)
    print "Avg requests per second: {0:.2f} [req/s]".format(num_sent/float(totaltime))
    print
    print "Percentage of the requests served within a certain time (ms)"
    timelist.sort()
    index = len(timelist) - 1
    print " 50%\t{0:.0f}".format(timelist[int(index*0.5)])
    print " 60%\t{0:.0f}".format(timelist[int(index*0.6)])
    print " 70%\t{0:.0f}".format(timelist[int(index*0.7)])
    print " 80%\t{0:.0f}".format(timelist[int(index*0.8)])
    print " 90%\t{0:.0f}".format(timelist[int(index*0.9)])
    print "100%\t{0:.0f}".format(timelist[int(index)])


def run_loop():
    try:
        asyncore.loop(timeout=5, use_poll=True)
    except (KeyboardInterrupt, SystemExit):
        print 'interrupt at ' + str(num_sent)
        sys.exit()


class OnePunch(asyncore.dispatcher):
    RECV_CHUNK_SIZE = 8192

    def __init__(self, url, i):
        asyncore.dispatcher.__init__(self)
        self.url = url
        scheme, host, path, port = parse_url(url)

        if scheme != 'http':
            print "must use http"
            sys.exit(2)
        self.i = i
        self.logger = logging.getLogger(str(i) + ' | ' + url)
        self.logger.debug('Created Object')
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.logger.info('Create socket on: ' + host + ':' + str(port))
        self.connect((host, port))
        self.sockfd = self.socket.fileno()
        self.host = host
        self.metainf = dict()
        self.got_head = False
        self.last = False
        self.recvlen = 0
        self.expectlen = 0
        self.start = 0.0
        self.end = 0.0
        (self.recvbuf, self.sendbuf) = (StringIO(), "")
        self.request = make_request(
            'GET', path, {
                'Host': host,
                'Connection': 'keep-alive',
                'P2Tag': 'u5580650_u5780590'
            }
        )
        self.last_request = make_request(
            'GET', path, {
                'Host': host,
                'Connection': 'close',
                'P2Tag': 'u5580650_u5780590'
            }
        )
        # print 'ok'
        # print request
        self.start = time.time()
        self.write(self.request)

    def get_socket(self):
        return self.socket.fileno()

    def write(self, data):
        self.sendbuf += data

    def handle_connect(self):
        self.logger.debug('Connected')

    def handle_close(self):
        self.logger.debug('Disconnected')
        self.close()

    # def handle_error(self):
    #     print 'someerror'
    #     self.logger.debug('Some Error')

    # def handle_expt(self):
    #     self.logger.debug('Some Exception')

    def writeable(self):
        return (len(self.sendbuf) > 0)

    def handle_write(self):
        bytes_sent = self.send(self.sendbuf)
        # self.logger.debug("sent {} bytes".format(bytes_sent))
        self.sendbuf = self.sendbuf[bytes_sent:]

    def handle_read(self):
        recv_bytes = self.recv(OnePunch.RECV_CHUNK_SIZE)
        self.logger.debug("recvd {} bytes".format(len(recv_bytes)))
        self.recvlen += len(recv_bytes)
        if not self.got_head:
            self.expectlen = 0
            self.recvbuf.write(recv_bytes)
            (
                metainf,
                headlength,
                content
            ) = extract_header(self.recvbuf.getvalue())
            if headlength != 0:
                self.metainf = metainf
                self.got_head = True
                self.expectlen = headlength + int(metainf['Content-Length'])
        # print str(self.i) + ' --- ' + str(num_sent) + ' . ' + str(num_total)
        if self.recvlen == self.expectlen and not self.last:
            global timelist
            timelist += [(time.time()-self.start)*1000]
            global num_sent
            num_sent += 1
            try:
                global num_fail
                if get_status(self.metainf.get('Status', 0)) != 2:
                    num_fail += 1
            except TypeError:
                num_fail += 1
            self.reset()
            if num_sent + num_con > num_total:
                self.last = True
                self.write(self.last_request)
            else:
                self.logger.debug("Received Full")
                self.write(self.request)

    def reset(self):
        self.got_head = False
        self.recvlen = 0
        self.expectlen = 0
        self.recvbuf.close()
        self.recvbuf = StringIO()
        self.metainf = dict()
        self.start = time.time()

if __name__ == '__main__':
    logging.basicConfig(
        filename='.punch.log',
        level=logging.DEBUG,
        format='%(asctime)-15s [%(levelname)s] %(name)s: %(message)s'
    )
    n = None
    c = None
    logging.info('Start punch')
    optlist, args = getopt.getopt(sys.argv[1:], 'n:c:u')
    for opt, arg in optlist:
        if opt in ('-n'):
            n = int(arg)
        if opt in ('-c'):
            c = int(arg)
    url = args[0]
    if c < n:
        num_con += c
    else:
        num_con += n
    num_total += n
    # def run_loop():
    try:
        make_punch(num_total, num_con, url)
    except KeyboardInterrupt:
        logging.info('KeyboardInterrupt')
        print 'KeyboardInterrupt'
        sys.exit(1)